﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class KillManager : MonoBehaviour
{
    public static int kill;


    Text text;


    void Awake()
    {
        text = GetComponent<Text>();
        kill = 0;
    }


    void Update()
    {
        print("KILLS:" + kill);
        text.text = "Kills: " + kill;
    }
}