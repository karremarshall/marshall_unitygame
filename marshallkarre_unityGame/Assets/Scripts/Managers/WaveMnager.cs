﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveMnager : MonoBehaviour
{
    public KillManager killManager;


    Animator anim;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (KillManager.kill > 1)
        {
            anim.SetTrigger("WaveCounter");
        }
    }
}
